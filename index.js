const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes")
const port = process.env.PORT || 4000;

const courseRoutes = require("./routes/courseRoutes")

const app = express();

mongoose.connect("mongodb+srv://Atanpogi:admin123@batch204-nuestrojonatha.vsomfie.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Naka kabit na sa MongoDB Atlas"))

app.use(express.json());

app.use("/users", userRoutes);
app.use("/courses", courseRoutes)

app.listen(port, ()=> {
	console.log(`ang API ay nakikinig sa ika-${port} na port`)
});